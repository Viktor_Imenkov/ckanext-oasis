import logging

import requests
import ckan.plugins.toolkit as tk

from requests.exceptions import ConnectionError
import pylons


config = pylons.config

log = logging.getLogger(__name__)
priv_domain = config.get('ckanext.oasis.django_priv_url')


def top_menu_widget():
    url = priv_domain + '/django-api/header-menu'
    try:
        top_menu = requests.get(url)
        return top_menu.json()
    except ConnectionError:
        log.debug('ConnectionError, cant connect to %s' % url)
        print('ConnectionError, cant connect to ', url)



def footer_news_widget():
    url = priv_domain + '/django-api/latest-news'
    try:
        footer_news = requests.get(url)
        return footer_news.json()
    except ConnectionError:
        log.debug('ConnectionError, cant connect to %s' % url)


def package_price(pkg_id):
    # todo return actual price, not id
    return 1.11

