import numbers

import ckan.plugins.toolkit as tk


# price field validator
def integer_or_float(value):
    if isinstance(value, numbers.Real):
        raise tk.Invalid("Not a number")
    return value