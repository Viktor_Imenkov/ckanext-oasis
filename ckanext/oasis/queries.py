import datetime
from decimal import Decimal as D

from slugify import slugify
from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy.types import Numeric
from sqlalchemy.ext.declarative import declarative_base

import ckan.model as model

Base = declarative_base()
session = model.Session()


class Product(Base):
    __tablename__ = 'catalogue_product'

    id = Column(Integer, primary_key=True)
    structure = Column(String, default='standalone')
    upc = Column(String, nullable=True)
    title = Column(String)
    slug = Column(String)
    description = Column(String, default='')
    date_created = Column(DateTime, default=datetime.datetime.utcnow)
    date_updated = Column(DateTime, default=datetime.datetime.utcnow)
    is_discountable = Column(Boolean, default=False)
    parent_id = Column(Integer, default=None)
    product_class_id = Column(String, default=1)
    package_id = Column(String, nullable=True)


class ResourceKeys(Base):
    __tablename__ = 'resource_keys'
    id = Column(Integer, primary_key=True)
    resource_id = Column(String, nullable=False)
    key = Column(String, nullable=False)



class StockRecord(Base):
    __tablename__ = 'partner_stockrecord'

    id = Column(Integer, primary_key=True)
    partner_sku = Column(default='')
    price_currency = Column(default='GBP')
    price_excl_tax = Column(Numeric(12, 2), nullable=True)
    price_retail = Column(Numeric(12, 2), nullable=True)
    cost_price = Column(Numeric(12, 2), nullable=True)
    num_in_stock = Column(Integer, nullable=True)
    num_allocated = Column(Integer, nullable=True)
    low_stock_threshold = Column(Integer, nullable=True)
    date_created = Column(DateTime, default=datetime.datetime.utcnow)
    date_updated = Column(DateTime, default=datetime.datetime.utcnow)
    partner_id = Column(Integer)
    product_id = Column(Integer)


class Partner(Base):
    __tablename__ = 'partner_partner'

    id = Column('id', Integer, primary_key=True)
    code = Column(String),
    name = Column(String),


def product_sync(package_id=None, title='', price=0):
    # make commit before we proceed
    session.commit()

    # check if partner exists
    parthner = session.query(Partner).filter().first()
    if not parthner:
        parthner = Partner(name='ckan', code='ckan')
        session.add(parthner)
        # TODO: we need retrieve partner.id for further usage
    # check if there is product for current package
    result = session.query(Product).filter(Product.package_id == package_id).count()
    if result:
        # update product
        product = session.query(Product).filter(Product.package_id == package_id).one()
        product.title = title
        # update price
        session.query(StockRecord).filter(StockRecord.product_id == product.id).\
            update({'price_excl_tax': D(price)})
        session.commit()
    else:
        # create product
        new_product = Product(title=title, slug=slugify(title), package_id=package_id)
        session.add(new_product)
        # we need commit, to get new_product.id
        session.commit()
        new_stock = StockRecord(price_excl_tax=D(price), partner_sku=package_id,
                                partner_id=1, product_id=new_product.id)
        session.add(new_stock)
        session.commit()
