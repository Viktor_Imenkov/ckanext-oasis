import logging

import pylons
from ckan import model
import ckan.plugins as p
import ckan.plugins.toolkit as tk

from .queries import product_sync
from .helpers import footer_news_widget, top_menu_widget, package_price


log = logging.getLogger(__name__)
config = pylons.config


class OasisPlugin(p.SingletonPlugin):

    p.implements(p.IConfigurer)
    p.implements(p.ITemplateHelpers)
    p.implements(p.IDomainObjectModification, inherit=True)

    # IConfigurer
    def update_config(self, config_):
        tk.add_template_directory(config_, 'templates')
        tk.add_public_directory(config_, 'public')
        tk.add_resource('fanstatic', 'oasis')

    def configure(self, config):
        # Certain config options must exists for the plugin to work. Raise an
        # exception if they're missing.
        missing_config = "{0} is not configured. Please amend your .ini file."
        config_options = (
            'ckanext.oasis.django_priv_url',
            'ckanext.oasis.django_pub_url'
        )
        for option in config_options:
            if not config.get(option, None):
                raise RuntimeError(missing_config.format(option))

    def get_helpers(self):
        pub_domain = config.get('ckanext.oasis.django_pub_url')
        return {
            'domain': pub_domain,
            'top_menu': top_menu_widget,
            'footer_news': footer_news_widget,
            'package_price': package_price,
            # 'resource_download_url': resource_download_url,
            # 'one_time_key_generate_url': one_time_key_generate_url
        }

    def notify(self, entity, operation=None):
        if not isinstance(entity, model.Package):
            return
        # log.debug('Notified of package event: %s %s', entity.id, operation)
        # sync product
        if entity.state == 'active':
            product_sync(entity.id, entity.title, entity.extras['price'])

