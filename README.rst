
# ckanext-oasis

## Additional Requirements:

1. `python-slugify`
2. `requests`


## Settings
Update your `ini` file (`/etc/ckan/default/development.ini`) with settings:
1. `ckanext.oasis.django_priv_url` private (inner) django url without backslash.
2. `ckanext.oasis.django_pub_url` public (website domain) django url without backslash.
